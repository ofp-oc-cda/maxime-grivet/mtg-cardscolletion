# Diagramme d'utilisation

```plantuml

"Administrateur" as Admin
"Utilisateur non enregistré" as Visiteur
"Utilisateur enregistré" as User

"Regarde les decks existant" as (Visite)
"S'identifie" as (Login)
"S'enregistre" as (Save)
"Regarde sa collection de cartes" as (Collec)
"Regarde sa collection de Decks" as (CollecDeck)
"Créer un nouveau deck" as (Build)
"Edite un deck" as (Edit)
"Playtest" as (Play)
"Ajoute des cartes" as (Ajout)
"Retire des cartes" as (Retire)
"Accès au forum" as (Forum)
"Créer un post sur le forum" as (CreateForum)
"Poste un message sur un sujet" as (PostMsg)
"Supprime un utilisateur" as (DeleteUser)
"Liste les utilisateurs" as (ListUser)
"Commente deck existant" as (CommentDeck)
"Update User" as (UpdateUser)
"Gère son profil" as (Profil)


Admin --> (ListUser)
Admin --> (DeleteUser)
Admin --> (UpdateUser)
User --> (Login)
Admin -->(Login)
(Login) --> (Profil)
(Forum) --> (CreateForum)
(Login) --> (Forum)
(Forum) --> (PostMsg)
(Login) --> (Visite)
(Login) --> (Collec)
(Collec) --> (Ajout)
(Collec) --> (Retire)
(Login) --> (CollecDeck)
(CollecDeck) --> (Build)
(CollecDeck) --> (Edit)
(Visite) --> (Play)
(Edit) --> (Play)
(Build) --> (Play)
Visiteur --> (Visite)
(Login) --> (CommentDeck)
Visiteur --> (Save)


```

# Diagramme de classe 

```plantuml
class Utilisateur{
 int id
 String nom
 String 
}
class Administrateur{
}
class Collection{
 int id
}
class Deck{
 int id
 String nom
}
class Carte{
 int id
 String nom
 String type
 int attaque
 int endurance
 String texte
}
class Message{
}
class Topic{
}

Utilisateur "0..*" -- "0..1" Collection
Utilisateur "0..*" -- "0..1" Message
Utilisateur "0..*" -- "*..*"Topic
Topic "0..*" -- "0..*" Message
Administrateur "1..*" --|> Utilisateur
Collection "0..*" -- "0..*" Deck
Collection "0..*" -- "0..*" Carte
Deck "0..*" -- "*..*" Carte
```
